from __future__ import division
import time
import RPi.GPIO as GPIO
import sys
import Adafruit_PCA9685

#Ultrasonic pins
Tr = 11
Ec = 8

pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm_freq(50)

# motor_EN_A: Pin7  |  motor_EN_B: Pin11
# motor_A:  Pin8,Pin10    |  motor_B: Pin13,Pin12

Motor_B_EN    = 4
Motor_A_EN    = 17

Motor_B_Pin1  = 14
Motor_B_Pin2  = 15
Motor_A_Pin1  = 27
Motor_A_Pin2  = 18

Dir_forward   = 0
Dir_backward  = 1

left_forward  = 0
left_backward = 1

right_forward = 0
right_backward= 1

pwn_A = 0
pwm_B = 0

speed_set = 70

line_pin_right = 19
line_pin_middle = 16
line_pin_left = 20

def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    #Line tracking sensor pins
    GPIO.setup(line_pin_right,GPIO.IN)
    GPIO.setup(line_pin_middle,GPIO.IN)
    GPIO.setup(line_pin_left,GPIO.IN)
    #Ultrasonic sensor
    GPIO.setup(Tr, GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(Ec, GPIO.IN)
    #motor.setup()
    global pwm_A, pwm_B
    GPIO.setup(Motor_A_EN, GPIO.OUT)
    GPIO.setup(Motor_B_EN, GPIO.OUT)
    GPIO.setup(Motor_A_Pin1, GPIO.OUT)
    GPIO.setup(Motor_A_Pin2, GPIO.OUT)
    GPIO.setup(Motor_B_Pin1, GPIO.OUT)
    GPIO.setup(Motor_B_Pin2, GPIO.OUT)

    motorStop()
    try:
        pwm_A = GPIO.PWM(Motor_A_EN, 1000)
        pwm_B = GPIO.PWM(Motor_B_EN, 1000)
    except:
        pass
    


def motorStop():#Motor stops
    GPIO.output(Motor_A_Pin1, GPIO.LOW)
    GPIO.output(Motor_A_Pin2, GPIO.LOW)
    GPIO.output(Motor_B_Pin1, GPIO.LOW)
    GPIO.output(Motor_B_Pin2, GPIO.LOW)
    GPIO.output(Motor_A_EN, GPIO.LOW)
    GPIO.output(Motor_B_EN, GPIO.LOW)



def motor_left(status, direction, speed):#Motor 2 positive and negative rotation
    if status == 0: # stop
        GPIO.output(Motor_B_Pin1, GPIO.LOW)
        GPIO.output(Motor_B_Pin2, GPIO.LOW)
        GPIO.output(Motor_B_EN, GPIO.LOW)
    else:
        if direction == Dir_backward:
            GPIO.output(Motor_B_Pin1, GPIO.HIGH)
            GPIO.output(Motor_B_Pin2, GPIO.LOW)
            pwm_B.start(100)
            pwm_B.ChangeDutyCycle(speed)
        elif direction == Dir_forward:
            GPIO.output(Motor_B_Pin1, GPIO.LOW)
            GPIO.output(Motor_B_Pin2, GPIO.HIGH)
            pwm_B.start(0)
            pwm_B.ChangeDutyCycle(speed)


def motor_right(status, direction, speed):#Motor 1 positive and negative rotation
    if status == 0: # stop
        GPIO.output(Motor_A_Pin1, GPIO.LOW)
        GPIO.output(Motor_A_Pin2, GPIO.LOW)
        GPIO.output(Motor_A_EN, GPIO.LOW)
    else:
        if direction == Dir_forward:#
            GPIO.output(Motor_A_Pin1, GPIO.HIGH)
            GPIO.output(Motor_A_Pin2, GPIO.LOW)
            pwm_A.start(100)
            pwm_A.ChangeDutyCycle(speed)
        elif direction == Dir_backward:
            GPIO.output(Motor_A_Pin1, GPIO.LOW)
            GPIO.output(Motor_A_Pin2, GPIO.HIGH)
            pwm_A.start(0)
            pwm_A.ChangeDutyCycle(speed)
    return direction


def move(speed, direction, turn, radius=0.6):   # 0 < radius <= 1  
    #speed = 100
    if direction == 'forward':
        if turn == 'right':
            motor_left(0, left_backward, int(speed*radius))
            motor_right(1, right_forward, speed)
        elif turn == 'left':
            motor_left(1, left_forward, speed)
            motor_right(0, right_backward, int(speed*radius))
        else:
            motor_left(1, left_forward, speed)
            motor_right(1, right_forward, speed)
    elif direction == 'backward':
        if turn == 'right':
            motor_left(0, left_forward, int(speed*radius))
            motor_right(1, right_backward, speed)
        elif turn == 'left':
            motor_left(1, left_backward, speed)
            motor_right(0, right_forward, int(speed*radius))
        else:
            motor_left(1, left_backward, speed)
            motor_right(1, right_backward, speed)
    elif direction == 'no':
        if turn == 'right':
            motor_left(1, left_backward, speed)
            motor_right(1, right_forward, speed)
        elif turn == 'left':
            motor_left(1, left_forward, speed)
            motor_right(1, right_backward, speed)
        else:
            motorStop()
    else:
        pass

def startServo():
   # pwm.set_pwm(11, 0, 240) #160 - 250 
    pwm.set_pwm(12, 0, 500) # 400 - 200 
    pwm.set_pwm(13, 0, 350) # 70 - 300 
   # pwm.set_pwm(14, 0, 300) # 100 - 500
    pwm.set_pwm(15, 0, 285) # 100 - 285


def destroy():
    motorStop()
    GPIO.cleanup()             # Release resource

def lineTracking():
    pwm.set_pwm(11, 0, 240)
    right = GPIO.input(line_pin_right)
    middle = GPIO.input(line_pin_middle)
    left = GPIO.input(line_pin_left)
    print('LF3: %d   LF2: %d   LF1: %d\n'%(right,middle,left))
    if middle == 1 and right == 0 and left == 0:
        move(speed_set, 'forward', 'no', 0.8)
    elif middle == 0 and right == 0 and left == 0:
        motorStop()
    elif middle == 1 and right == 1 and left == 1:
        motorStop()
    elif middle == 0 and right == 0 and left == 1:
        motor_left(1,Dir_backward,speed_set)
        motor_right(1,Dir_forward,speed_set)
    elif middle == 0 and right == 1 and left == 0:
        motor_left(1,Dir_forward,speed_set)
        motor_right(1,Dir_backward,speed_set)    
    elif middle == 1 and right == 0 and left == 1:
        motor_left(1,Dir_backward,speed_set)
        motor_right(1,Dir_forward,speed_set)
    elif middle == 1 and right == 1 and left == 0:
        motor_left(1,Dir_forward,speed_set)
        motor_right(1,Dir_backward,speed_set)

def lineObstacle():
    pwm.set_pwm(11, 0, 240)
    right = GPIO.input(line_pin_right)
    middle = GPIO.input(line_pin_middle)
    left = GPIO.input(line_pin_left)
    
    distance = checkdist()
    
    if distance < 10 :
        motorStop()
        move(speed_set, 'backward', 'no', 0.8)
        time.sleep(0.1)
        motorStop()
        robotArm()
        time.sleep(0.1)
    else:
        lineTracking()
            
    
'''def robotArm():
    for a in range(350,200,-1):
        pwm.set_pwm(13, 0, a)
        time.sleep(0.01)
    
    for a in range(500,175,-1):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)
        
    for a in range(285,100,-1):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)

    pwm.set_pwm(12, 0, 140)
    pwm.set_pwm(13, 0, 200)
    time.sleep(1)
    for a in range(100,235):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)

    pwm.set_pwm(12, 0, 500)
    pwm.set_pwm(13, 0, 350) 
    ##     
    time.sleep(0.5)
    motor_left(1,Dir_forward,speed_set)
    motor_right(1,Dir_backward,speed_set)
    time.sleep(0.6)
    motorStop()
    time.sleep(0.5)   
    ##

    
    pwm.set_pwm(13, 0, 200)
    
    
    #pwm.set_pwm(12, 0, 250)
    for a in range(500,250,-1):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)


    time.sleep(1)

    pwm.set_pwm(15, 0, 100)
    


    time.sleep(0.5)
    motor_left(1,Dir_backward,speed_set)
    motor_right(1,Dir_forward,speed_set)
    time.sleep(0.6)
    motorStop()
    time.sleep(0.5)
''' 

def robotArm():

    for a in range(500,175,-1):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)

    for a in range(350,200,-1):
        pwm.set_pwm(13, 0, a)
        time.sleep(0.01)
        
    '''          
    
            
    for a in range(285,100,-1):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)
    
    for a in range(175,140,-1):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.008)
    
    for a in range(100,235):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)

    for a in range(200,350):
        pwm.set_pwm(13, 0, a)
        time.sleep(0.01)
        
    for a in range(175,500):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)
    '''
           
    
    time.sleep(0.5)
    motor_left(1,Dir_forward,speed_set)
    motor_right(1,Dir_backward,speed_set)
    time.sleep(0.6)
    motorStop()
    time.sleep(0.5)
    
    
    '''for a in range(350,200,-1):
        pwm.set_pwm(13, 0, a)
        time.sleep(0.01)
        
    for a in range(500,250,-1):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)
        
    for a in range(235,150,-1):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)
    
    for a in range(150,285):
        pwm.set_pwm(15, 0, a)
        time.sleep(0.01)
    
    for a in range(250,500):
        pwm.set_pwm(12, 0, a)
        time.sleep(0.01)
    
    for a in range(200,350):
        pwm.set_pwm(13, 0, a)
        time.sleep(0.01)
    '''
    time.sleep(0.5)
    motor_left(1,Dir_backward,speed_set)
    motor_right(1,Dir_forward,speed_set)
    time.sleep(0.6)
    motorStop()
    time.sleep(0.5)

def checkdist():       #Reading distance
    for i in range(5):  # Remove invalid test results.
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(Tr, GPIO.OUT,initial=GPIO.LOW)
        GPIO.setup(Ec, GPIO.IN)
        GPIO.output(Tr, GPIO.LOW)
        time.sleep(0.000002)
        GPIO.output(Tr, GPIO.HIGH)
        time.sleep(0.000015)
        GPIO.output(Tr, GPIO.LOW)
        while not GPIO.input(Ec):
            pass
        t1 = time.time()
        while GPIO.input(Ec):
            pass
        t2 = time.time()
        dist = (t2-t1)*340/2
        if dist > 9 and i < 4:  # 5 consecutive times are invalid data, return the last test data
            continue
        else:
            return dist*100
        
def cuteRobot():
    distance = checkdist()
    pwm.set_pwm(12, 0, 400) 
    pwm.set_pwm(13, 0, 280) 
  #  pwm.set_pwm(14, 0, 300) 
    pwm.set_pwm(15, 0, 285) 
    
    if distance < 9:
        move(speed_set, 'backward', 'no', 0.8)
        for i in range(190,210):
            pwm.set_pwm(11, 0, i)
            time.sleep(0.01)
        for i in range(210,190,-1):
            pwm.set_pwm(11, 0, i)
            time.sleep(0.01)
    else:
        move(speed_set, 'forward', 'no', 0.8)
        print("forward")

     
def servoS():
    pwm.set_pwm(12, 0, 500)
    pwm.set_pwm(13, 0, 350)
    #pwm.set_pwm(14, 0, 300)
    pwm.set_pwm(15, 0, 285)
    
    

if __name__ == '__main__': 
    try:     
        setup()
        servoS()
        while True:           
            lineObstacle()
#            cuteRobot()
#            startServo()
#            lineTracking()
    except KeyboardInterrupt:
        destroy()
